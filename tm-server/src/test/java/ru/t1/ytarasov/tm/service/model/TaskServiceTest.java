package ru.t1.ytarasov.tm.service.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.ytarasov.tm.api.service.IConnectionService;
import ru.t1.ytarasov.tm.api.service.IPropertyService;
import ru.t1.ytarasov.tm.api.service.model.IProjectService;
import ru.t1.ytarasov.tm.api.service.model.IProjectTaskService;
import ru.t1.ytarasov.tm.api.service.model.ITaskService;
import ru.t1.ytarasov.tm.api.service.model.IUserService;
import ru.t1.ytarasov.tm.enumerated.Role;
import ru.t1.ytarasov.tm.enumerated.Status;
import ru.t1.ytarasov.tm.exception.entity.TaskNotFoundException;
import ru.t1.ytarasov.tm.exception.field.*;
import ru.t1.ytarasov.tm.marker.UnitCategory;
import ru.t1.ytarasov.tm.model.Project;
import ru.t1.ytarasov.tm.model.Task;
import ru.t1.ytarasov.tm.model.User;
import ru.t1.ytarasov.tm.service.ConnectionService;
import ru.t1.ytarasov.tm.service.PropertyService;

import java.util.List;

@Category(UnitCategory.class)
public class TaskServiceTest {

    @NotNull
    private static final String TEST_TASK_NAME = "test";

    @NotNull
    private static final String TEST_TASK_DESCRIPTION = "test";

    @NotNull
    private static final String TEST_ADD_TASK_NAME = "test add";

    @NotNull
    private static final String TEST_ADD_TASK_DESCRIPTION = "test add";

    @NotNull
    private static final String TEST_CREATE_TASK_NAME = "test create";

    @NotNull
    private static final String TEST_CREATE_TASK_DESCRIPTION = "test create";

    @NotNull
    private static final String TEST_REMOVE_TASK_NAME = "test remove";

    @NotNull
    private static final String TEST_REMOVE_TASK_DESCRIPTION = "test remove";
    @NotNull
    private static final String TEST_BIND_TO_PROJECT_NAME = "bind to project";

    @NotNull
    private static final String TEST_BIND_TO_PROJECT_DESCRIPTION = "bind to project";

    @NotNull
    private static final IPropertyService propertyService = new PropertyService();

    @NotNull
    private static final IConnectionService connectionService = new ConnectionService(propertyService);

    @NotNull
    private static final IUserService userService = new UserService(connectionService, propertyService);

    @NotNull
    private static final IProjectService projectService = new ProjectService(connectionService);

    @NotNull
    private static final ITaskService taskService = new TaskService(connectionService);

    @NotNull
    private static final IProjectTaskService projectTaskService = new ProjectTaskService(connectionService);

    private static User user;

    private static Project project;

    private static String taskId;

    @Nullable
    private final Task nullTask = null;

    @BeforeClass
    public static void setUp() throws Exception {
        user = userService.create("test tasks", "test tasks", "test_tasks@tst.ru", Role.USUAL);
        project = projectService.create(user.getId(), "test binding", "test binding");
        @NotNull final Task task = taskService.create(user.getId(), TEST_TASK_NAME, TEST_TASK_DESCRIPTION);
        taskId = task.getId();
    }

    @AfterClass
    public static void tearDown() throws Exception {
        taskService.clear();
        projectService.remove(project);
        userService.remove(user);
    }

    @Test
    public void getSize() throws Exception {
        @Nullable final List<Task> tasks = taskService.findAll();
        Assert.assertNotNull(tasks);
        final int expectedSize = tasks.size();
        final int foundSize = taskService.getSize().intValue();
        Assert.assertEquals(expectedSize, foundSize);
    }

    @Test
    public void findAll() throws Exception {
        final int expectedSize = taskService.getSize().intValue();
        @Nullable final List<Task> tasks = taskService.findAll();
        Assert.assertNotNull(tasks);
        final int foundSize = tasks.size();
        Assert.assertEquals(expectedSize, foundSize);
    }

    @Test
    public void findOneById() throws Exception {
        Assert.assertThrows(IdEmptyException.class, () -> taskService.findOneById(null));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.findOneById(""));
        @Nullable final Task task = taskService.findOneById(taskId);
        Assert.assertNotNull(task);
        Assert.assertEquals(TEST_TASK_NAME, task.getName());
    }

    @Test
    public void add() throws Exception {
        Assert.assertThrows(TaskNotFoundException.class, () -> taskService.add(nullTask));
        final int expectedSize = taskService.getSize().intValue() + 1;
        taskService.add(new Task(user, TEST_ADD_TASK_NAME, TEST_ADD_TASK_DESCRIPTION));
        final int foundSize = taskService.getSize().intValue();
        Assert.assertEquals(expectedSize, foundSize);
    }

    @Test
    public void create() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.create(null, TEST_CREATE_TASK_NAME, TEST_CREATE_TASK_DESCRIPTION));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.create("", TEST_CREATE_TASK_NAME, TEST_CREATE_TASK_DESCRIPTION));
        Assert.assertThrows(NameEmptyException.class, () -> taskService.create(user.getId(), null, TEST_CREATE_TASK_DESCRIPTION));
        Assert.assertThrows(NameEmptyException.class, () -> taskService.create(user.getId(), "", TEST_CREATE_TASK_DESCRIPTION));
        Assert.assertThrows(DescriptionEmptyException.class, () -> taskService.create(user.getId(), TEST_CREATE_TASK_NAME, null));
        Assert.assertThrows(DescriptionEmptyException.class, () -> taskService.create(user.getId(), TEST_CREATE_TASK_NAME, ""));
        final int expectedSize = taskService.getSize().intValue() + 1;
        taskService.create(user.getId(), TEST_CREATE_TASK_NAME, TEST_CREATE_TASK_DESCRIPTION);
        final int foundSize = taskService.getSize().intValue();
        Assert.assertEquals(expectedSize, foundSize);
    }

    @Test
    public void changeStatus() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.changeStatusById(null, taskId, Status.COMPLETED));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.changeStatusById("", taskId, Status.COMPLETED));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.changeStatusById(user.getId(), null, Status.COMPLETED));
        Assert.assertThrows(IdEmptyException.class, () -> taskService.changeStatusById(user.getId(), "", Status.COMPLETED));
        Assert.assertThrows(StatusEmptyException.class, () -> taskService.changeStatusById(user.getId(), taskId, null));
        taskService.changeStatusById(user.getId(), taskId, Status.COMPLETED);
        @Nullable final Task changedTask = taskService.findOneById(user.getId(), taskId);
        Assert.assertNotNull(changedTask);
        Assert.assertEquals(Status.COMPLETED.getDisplayName(), changedTask.getStatus().getDisplayName());
    }

    @Test
    public void bindTask() throws Exception {
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.bindTaskToProject(null, taskId, project.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> projectTaskService.bindTaskToProject("", taskId, project.getId()));
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.bindTaskToProject(user.getId(), null, project.getId()));
        Assert.assertThrows(TaskIdEmptyException.class, () -> projectTaskService.bindTaskToProject(user.getId(), "", project.getId()));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.bindTaskToProject(user.getId(), taskId, null));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> projectTaskService.bindTaskToProject(user.getId(), taskId, ""));
        @NotNull final Task task = taskService.create(user.getId(), TEST_BIND_TO_PROJECT_NAME, TEST_BIND_TO_PROJECT_DESCRIPTION);
        projectTaskService.bindTaskToProject(user.getId(), task.getId(), project.getId());
        @Nullable final Task task1 = taskService.findOneById(user.getId(), task.getId());
        Assert.assertNotNull(task1);
        @Nullable final Project foundProject = task1.getProject();
        Assert.assertNotNull(foundProject);
        @Nullable final String foundProjectId = foundProject.getId();
        Assert.assertNotNull(foundProjectId);
        Assert.assertEquals(project.getId(), foundProjectId);
    }

    @Test
    public void remove() throws Exception {
        Assert.assertThrows(TaskNotFoundException.class, () -> taskService.remove(nullTask));
        @NotNull final Task task = taskService.create(user.getId(), TEST_REMOVE_TASK_NAME, TEST_REMOVE_TASK_DESCRIPTION);
        final int expectedSize = taskService.getSize().intValue() - 1;
        taskService.remove(task);
        final int foundSize = taskService.getSize().intValue();
        Assert.assertEquals(expectedSize, foundSize);
    }

    @Test
    public void removeWithUserId() throws Exception {
        Assert.assertThrows(TaskNotFoundException.class, () -> taskService.remove(user.getId(), nullTask));
        @NotNull final Task task = taskService.create(user.getId(), TEST_REMOVE_TASK_NAME + "ID", TEST_REMOVE_TASK_DESCRIPTION + "ID");
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.remove(null, task));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.remove("", task));
        final int expectedSize = taskService.getSize().intValue() - 1;
        taskService.remove(user.getId(), task);
        final int foundSize = taskService.getSize().intValue();
        Assert.assertEquals(expectedSize, foundSize);
    }

    @Test
    public void clear() throws Exception {
        taskService.clear();
        Assert.assertEquals(0, taskService.getSize().intValue());
        taskService.create(user.getId(), "test binding", "test binding");
    }

    @Test
    public void findByProjectId() throws Exception {
        Assert.assertThrows(ProjectIdEmptyException.class, () -> taskService.findByProjectId(user.getId(), null));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> taskService.findByProjectId(user.getId(), ""));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findByProjectId(null, project.getId()));
        Assert.assertThrows(UserIdEmptyException.class, () -> taskService.findByProjectId("", project.getId()));
        @NotNull final Task task = new Task();
        task.setName("test find by pr");
        task.setDescription("test find by pr");
        task.setProject(project);
        taskService.add(user.getId(), task);
        @Nullable final List<Task> tasks = taskService.findByProjectId(user.getId(), project.getId());
        Assert.assertNotNull(tasks);
        Assert.assertFalse(tasks.isEmpty());
    }

}
