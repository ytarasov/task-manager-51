package ru.t1.ytarasov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.EntityConstantDTO;
import ru.t1.ytarasov.tm.api.repository.dto.IUserOwnedRepositoryDTO;
import ru.t1.ytarasov.tm.dto.model.AbstractUserOwnedModelDTO;

import javax.persistence.EntityManager;
import java.util.List;

public abstract class AbstractUserOwnedRepositoryDTO<M extends AbstractUserOwnedModelDTO>
        extends AbstractRepositoryDTO<M> implements IUserOwnedRepositoryDTO<M> {

    public AbstractUserOwnedRepositoryDTO(@NotNull EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public void clear(@NotNull final String userId) {
        @Nullable final List<M> models = findAll(userId);
        if (models == null || models.isEmpty()) return;
        for (@NotNull final M model : models) remove(model);
    }

}
