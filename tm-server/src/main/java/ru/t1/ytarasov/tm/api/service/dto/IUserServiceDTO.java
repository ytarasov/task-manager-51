package ru.t1.ytarasov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.enumerated.Role;
import ru.t1.ytarasov.tm.dto.model.UserDTO;

import java.util.List;

public interface IUserServiceDTO extends IServiceDTO<UserDTO> {
    @NotNull
    UserDTO create(
            @Nullable String login,
            @Nullable String password,
            @Nullable final String email,
            @Nullable Role role
    ) throws Exception;

    @Nullable
    UserDTO findByLogin(@Nullable String login) throws Exception;

    @Nullable
    UserDTO findByEmail(@Nullable String email) throws Exception;

    @Nullable
    UserDTO removeByLogin(@Nullable String login) throws Exception;

    @Nullable
    UserDTO removeByEmail(@Nullable String email) throws Exception;

    @NotNull
    UserDTO setPassword(@Nullable String id, @Nullable String password) throws Exception;

    @NotNull
    UserDTO updateUser(@Nullable String id, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName) throws Exception;

    boolean isLoginExist(@Nullable String login) throws Exception;

    boolean isEmailExists(@Nullable String email) throws Exception;

    void lockUser(@Nullable String login) throws Exception;

    void unlockUser(@Nullable String login) throws Exception;

}
