package ru.t1.ytarasov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.EntityConstantDTO;
import ru.t1.ytarasov.tm.api.repository.dto.IRepositoryDTO;
import ru.t1.ytarasov.tm.comparator.CreatedComparator;
import ru.t1.ytarasov.tm.comparator.NameComparator;
import ru.t1.ytarasov.tm.dto.model.AbstractModelDTO;

import javax.persistence.EntityManager;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractRepositoryDTO<M extends AbstractModelDTO> implements IRepositoryDTO<M> {

    @NotNull
    protected final EntityManager entityManager;

    public AbstractRepositoryDTO(@NotNull EntityManager entityManager) {
        this.entityManager = entityManager;
    }

    @NotNull
    protected abstract String getTableName();

    @NotNull
    protected String getSortedColumn(@NotNull final Comparator comparator) {
        if (comparator == NameComparator.INSTANCE) return EntityConstantDTO.COLUMN_NAME;
        else if (comparator == CreatedComparator.INSTANCE) return EntityConstantDTO.COLUMN_CREATED;
        else return EntityConstantDTO.COLUMN_STATUS;
    }

    @Override
    public void add(@NotNull final M model) {
        entityManager.persist(model);
    }

    @Override
    public void remove(@NotNull final M model) {
        entityManager.remove(model);
    }

    @Override
    public void update(@NotNull final M model) {
        entityManager.merge(model);
    }

    @Override
    public void clear() {
        @Nullable final List<M> models = findAll();
        if (models == null || models.isEmpty()) return;
        for (@NotNull final M model : models) remove(model);
    }

}
