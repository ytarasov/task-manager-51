package ru.t1.ytarasov.tm.component;

import lombok.Getter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.ytarasov.tm.api.endpoint.*;
import ru.t1.ytarasov.tm.api.service.*;
import ru.t1.ytarasov.tm.api.service.dto.*;
import ru.t1.ytarasov.tm.api.service.model.IProjectService;
import ru.t1.ytarasov.tm.api.service.model.IProjectTaskService;
import ru.t1.ytarasov.tm.api.service.model.ITaskService;
import ru.t1.ytarasov.tm.api.service.model.IUserService;
import ru.t1.ytarasov.tm.endpoint.*;
import ru.t1.ytarasov.tm.service.*;
import ru.t1.ytarasov.tm.service.dto.*;
import ru.t1.ytarasov.tm.service.model.ProjectService;
import ru.t1.ytarasov.tm.service.model.ProjectTaskService;
import ru.t1.ytarasov.tm.service.model.TaskService;
import ru.t1.ytarasov.tm.service.model.UserService;
import ru.t1.ytarasov.tm.util.SystemUtil;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;

public final class Bootstrap implements IServiceLocator {

    @Getter
    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @Getter
    @NotNull
    private final IConnectionService connectionService = new ConnectionService(propertyService);

    @Getter
    @NotNull
    private final IProjectServiceDTO projectServiceDTO = new ProjectServiceDTO(connectionService);

    @Getter
    @NotNull
    private final ITaskServiceDTO taskServiceDTO = new TaskServiceDTO(connectionService);

    @Getter
    @NotNull
    private final IProjectTaskServiceDTO projectTaskServiceDTO = new ProjectTaskServiceDTO(projectServiceDTO, taskServiceDTO);

    @Getter
    @NotNull
    private final ISessionServiceDTO sessionServiceDTO = new SessionServiceDTO(connectionService);

    @Getter
    @NotNull
    private final IUserServiceDTO userServiceDTO = new UserServiceDTO(connectionService, propertyService);

    @Getter
    @NotNull
    private final IAuthService authService = new AuthService(propertyService, userServiceDTO, sessionServiceDTO);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(connectionService);

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(connectionService);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(connectionService, propertyService);

    @Getter
    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(connectionService);

    @Getter
    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final ISystemEndpoint systemEndpoint = new SystemEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IAuthEndpoint authEndpoint = new AuthEndpoint(this);

    {
        registry(authEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
        registry(systemEndpoint);
        registry(userEndpoint);
    }

    private void registry(@NotNull final Object endpoint) {
        @NotNull final String host = propertyService.getHost();
        @NotNull final String port = propertyService.getPort();
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String url = "http://" + host + ":" + port + "/" + name + "?wsdl";
        Endpoint.publish(url, endpoint);
        System.out.println(url);
    }

    private void prepareStart() {
        initPid();
        connectionService.initLogger();
        loggerService.info("** WELCOME TO THE TASK MANAGER SERVER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
    }

    private void prepareShutdown() {
        loggerService.info("** TASK MANAGER SERVER IS SHUTTING DOWN **");
    }

    public void run() {
        prepareStart();
    }

    @SneakyThrows
    private void initPid() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPid());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

}
