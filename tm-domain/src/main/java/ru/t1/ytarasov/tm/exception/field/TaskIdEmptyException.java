package ru.t1.ytarasov.tm.exception.field;

public final class TaskIdEmptyException extends AbstractFieldException {

    public TaskIdEmptyException() {
        super("Error! TaskDTO ID is empty...");
    }

}
