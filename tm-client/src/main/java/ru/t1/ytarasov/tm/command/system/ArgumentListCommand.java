package ru.t1.ytarasov.tm.command.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ytarasov.tm.api.model.ICommand;
import ru.t1.ytarasov.tm.command.AbstractCommand;
import ru.t1.ytarasov.tm.exception.AbstractException;

import java.util.Collection;

public final class ArgumentListCommand extends AbstractSystemCommand {

    @NotNull
    public static final String NAME = "argument";

    @NotNull
    public static final String ARGUMENT = "-arg";

    @NotNull
    public static final String DESCRIPTION = "Show argument list";

    @Override
    public void execute() throws AbstractException {
        System.out.println("[ARGUMENTS]");
        @Nullable final Collection<AbstractCommand> commands = getCommandService().getTerminalCommands();
        if (commands == null) return;
        for (@NotNull ICommand command : commands) {
            @Nullable final String argument = command.getArgument();
            if (argument == null || argument.isEmpty()) continue;
            System.out.println(argument);
        }
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @NotNull
    @Override
    public String getArgument() {
        return ARGUMENT;
    }

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

}
